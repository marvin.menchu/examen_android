package com.umg.maestria.ejecla;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText nombreEt;
    private EditText apellidoEt;
    private EditText passwordEt;
    private EditText confirmEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nombreEt = (EditText) findViewById(R.id.nombreId);
        apellidoEt = (EditText) findViewById(R.id.apellidoId);
        passwordEt = (EditText) findViewById(R.id.passwordId);
        confirmEt = (EditText) findViewById(R.id.confirmId);

    }


    public void guardar(View view){

        String nom = nombreEt.getText().toString();
        String ape = apellidoEt.getText().toString();
        String pas = passwordEt.getText().toString();
        String con = confirmEt.getText().toString();

        if (nom.trim().equals("") || ape.trim().toString().equals("") || pas.trim().toString().equals("") || con.trim().toString().equals("")){
            Toast notificacion= Toast.makeText(this,"Ups! Debe de completar el formulario",Toast.LENGTH_SHORT);
            notificacion.show();
        }else if (!pas.equals(con)){
            Toast passVer= Toast.makeText(this,"Credenciales inválidas!",Toast.LENGTH_SHORT);
            passVer.show();
        }else{
            Intent i = new Intent(this, Intereses.class);
            startActivity(i);
        }

    }

    public void cancelar(View view){
        nombreEt.setText("");
        apellidoEt.setText("");
        passwordEt.setText("");
        confirmEt.setText("");
        nombreEt.requestFocus();
    }

}
