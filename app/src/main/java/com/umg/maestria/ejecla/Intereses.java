package com.umg.maestria.ejecla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Intereses extends AppCompatActivity {

    private RadioButton androidS;
    private RadioButton androidN;
    private RadioButton javaS;
    private RadioButton javaN;
    private RadioButton springS;
    private RadioButton springN;

    private List<String> listaLenguajes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intereses);
        listaLenguajes = new ArrayList<>();

        androidS = (RadioButton) findViewById(R.id.androidS);
        androidN = (RadioButton) findViewById(R.id.androidN);
        javaS = (RadioButton) findViewById(R.id.javaS);
        javaN = (RadioButton) findViewById(R.id.javaN);
        springS = (RadioButton) findViewById(R.id.springS);
        springN = (RadioButton) findViewById(R.id.springN);

    }


    public void enviar(View view){
        Toast passVer= Toast.makeText(this,"Preferencias listas para monitoreo, lenguajes: " + listaLenguajes.toString(), Toast.LENGTH_SHORT);
        passVer.show();

    }

    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.androidS:
                if(checked)
                    listaLenguajes.add("Android");
                break;
            case R.id.javaS:
                if(checked)
                    listaLenguajes.add("Java");
                break;
            case R.id.springS:
                if(checked)
                    listaLenguajes.add("Spring");
                break;
        }
    }

    public void onRadioButtonClickedRemove(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.androidN:
                if(checked)
                    listaLenguajes.remove("Android");
                break;
            case R.id.javaN:
                if(checked)
                    listaLenguajes.remove("Java");
                break;
            case R.id.springN:
                if(checked)
                    listaLenguajes.remove("Spring");
                break;
        }
    }
}
